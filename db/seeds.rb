# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)
materials = MaterialClass.create(
  [
    {name: 'Wax'}, 
    {name: 'antioxydant'},
    {name: 'vegetable oil'},
    {name: 'mineral oil'},
    {name: 'polymer'}, 
    {name: 'resin'},
    {name: 'extract'},
    {name: 'compound'},
    {name: 'liquid PBD'},
    {name: 'TDAE Oil'},
    {name: 'AMS resin'},
    {name: 'Veg. Oil'},
    {name: 'Indene-Coumarone'}
  ]
)
projects = Project.create([
    {name: 'GYL-3.2.1',description: ''},
    {name: 'GYL-3.2.2',description: :SmaCro},
    {name: 'GYL-4.2.1', description: ''},
    {name: 'GYL-4.2.2', description: :RawMan}
    ])