class CreateMaterialClasses < ActiveRecord::Migration
  def change
    create_table :material_classes do |t|
      t.string :name
      t.text :description
 
      t.timestamps
    end
  end
end
