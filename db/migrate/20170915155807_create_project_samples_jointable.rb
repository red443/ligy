class CreateProjectSamplesJointable < ActiveRecord::Migration
  def change
    create_join_table :project, :sample, table_name: :project_samples
  end
end
