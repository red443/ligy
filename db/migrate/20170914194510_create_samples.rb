class CreateSamples < ActiveRecord::Migration
  def change
    create_table :samples do |t|
      t.string :tradename
      t.string :batch
      t.belongs_to :parent, foreign_key:{to_table: :samples}
      t.string :origin
      t.belongs_to :materialClass
      t.text :comment
      t.string :provider
      t.belongs_to :primaryProject, foreign_key:{to_table: :projects}

      t.timestamps null: false
    end
  end
end
