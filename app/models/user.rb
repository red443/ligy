class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable
  has_paper_trail 
  def is_goodyear?
    if /@goodyear\.com/.match(email)
      ret = true
    else
      ret = false
    end
    logger.debug "isgoodyear= #{ret}"
    ret
  end
end
