class Sample < ActiveRecord::Base
  #validates :name, presence: true, uniqueness: true
  validates :tradename, presence: true
  validates :batch, presence: true
  validates_uniqueness_of :batch, scope: :tradename
  validates :parent_id, inclusion: {in: Sample.ids, message: "must be a registered batch - is #{parent}"}, allow_nil: true
  validates :primaryProject_id, inclusion: {in: Project.ids, message: 'must be an existing project'}, allow_nil: true
  validates :materialClass_id, inclusion: {in: MaterialClass.ids, message: 'must be a registered material class'}
  belongs_to :parent, class_name: 'Sample'
  has_many :children, class_name: 'Sample', :foreign_key => 'parent_id'
  belongs_to :materialClass
  has_and_belongs_to_many :projects, join_table: :project_samples
  belongs_to :primaryProject, class_name: 'Project'
  has_paper_trail 
  
  def tradename_and_batch
    "#{tradename}-#{batch}"
  end
end
