class Project < ActiveRecord::Base
  #has_many :samples
  has_and_belongs_to_many :samples, join_table: :project_samples
  has_paper_trail 
  def name_and_code
    "#{name}-#{description}"
  end
end
